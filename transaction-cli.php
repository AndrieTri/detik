<?php

$servername = "127.0.0.1";
$username = "root";
$password = "root";
$dbname = "detik";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

if (isset($argc)) {
    if(isset($argv[1])){
        if(isset($argv[2])){
            $sql = "UPDATE invoices SET status='$argv[2]' WHERE references_id=$argv[1]";
        }else{
            echo "Please input status parameter\n";
        }
    }else{
        echo "Please input references_id parameter\n";
    }
}else {
	echo "Please input parameter\n";
}

if(!empty($sql)){
    if ($conn->query($sql) === TRUE) {
        echo "Record updated successfully";
    } else {
        echo "Error updating record: " . $conn->error;
    }
}else{
    echo "Error updating record\n";
}

$conn->close();