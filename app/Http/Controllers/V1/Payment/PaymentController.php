<?php

namespace App\Http\Controllers\V1\Payment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Eloquent\PaymentRepositoryEloquent;
use App\Criteria\PaymentFilterCriteria;

class PaymentController extends Controller
{
    public function __construct(PaymentRepositoryEloquent $repository)
    {
        $this->repository = $repository;
    }

    public function get()
    {
        $payment = $this->repository
                    ->setPresenter("App\\Presenters\\PaymentPresenterGet")
                    ->getByCriteria(new PaymentFilterCriteria());
        
        return $this->responseSuccess($payment);
    }

    public function create(Request $request)
    {
        $payment = $this->repository
                    ->setPresenter("App\\Presenters\\PaymentPresenterCreate")
                    ->createCustom($request->all());

        return $this->responseCreated($payment);
    }
}