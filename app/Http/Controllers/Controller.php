<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * @var Illuminate\Http\Request
     */
    protected $request;

    /**
     * Status code
     *
     * @var int
     */
    protected $statusCode = 0;

    /**
     * Status message
     *
     * @var string
     */
    protected $statusMessage = 'Success';

    /**
     * Http status code
     *
     * @var int
     */
    protected $httpStatusCode = Response::HTTP_OK;

    protected function responseCreated($data)
    {
        return $this
            ->setStatusMessage('Created')
            ->setStatusCode(0)
            ->setHttpStatusCode(Response::HTTP_CREATED)
            ->responseSuccess($data);
    }

    protected function responseUpdated($data)
    {
        return $this
            ->setStatusMessage('Updated')
            ->setStatusCode(0)
            ->setHttpStatusCode(Response::HTTP_ACCEPTED)
            ->responseSuccess($data);
    }

    protected function responseDeleted($data)
    {
        return $this
            ->setStatusMessage('Deleted')
            ->setStatusCode(0)
            ->setHttpStatusCode(Response::HTTP_ACCEPTED)
            ->responseSuccess($data);
    }

    protected function responseError($data)
    {
        return $this
            ->setStatusMessage('Error')
            ->setStatusCode(1)
            ->setHttpStatusCode(Response::HTTP_NOT_FOUND)
            ->responseSuccess($data);
    }

    protected function responseErrorValidation($message, $data = null)
    {
        return $this
            ->setStatusMessage($message)
            ->setStatusCode(1)
            ->setHttpStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->responseSuccess($data);
    }

    protected function responseSuccess($data = [])
    {
        if(empty($data['data'])){
            $data = [
                'data' => $data
            ];
        }
        
        if(empty($data)){
            $data = [
                'data' => null
            ];
        }
        
        return response()->json(array_merge($this->getStatusInfo(), $data), $this->httpStatusCode);
    }

    private function getStatusInfo()
    {
        return [
            'status' => [
                'code'    => $this->statusCode,
                'message' => $this->statusMessage
            ]
        ];
    }

    protected function setStatusCode(int $statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    protected function setStatusMessage(string $message)
    {
        $this->statusMessage = $message;

        return $this;
    }

    protected function setHttpStatusCode(int $httpStatusCode)
    {
        $this->httpStatusCode = $httpStatusCode;

        return $this;
    }
}
