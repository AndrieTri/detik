<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class CustomerFavoriteCriteria.
 *
 * @package namespace App\Criteria;
 */
class PaymentFilterCriteria implements CriteriaInterface
{
    public function __construct()
    {
        //
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $referencesId = app('request')->get('references_id');
        $merchantId = app('request')->get('merchant_id');

        $model = $model->filterByReferencesId($referencesId)
                        ->filterByMerchantId($merchantId);

        return $model;
    }
}