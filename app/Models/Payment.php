<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Payment.
 *
 * @package namespace App\Models;
 */
class Payment extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'invoices';
    protected $primaryKey = 'references_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    
    protected $fillable = [
        'invoice_id',
        'item_name',
        'amount',
        'payment_type',
        'customer_name',
        'merchant_id',
        'status',
        'number_va'
    ];

    protected $dates = [
        'created_date',
        'updated_date'
    ];

    public function scopeFilterByReferencesId($query, $referencesId)
    {
        if (is_null($referencesId) || empty($referencesId)) return $query;

        return $query->where('references_id', $referencesId);
    }

    public function scopeFilterByMerchantId($query, $merchantId)
    {
        if (is_null($merchantId) || empty($merchantId)) return $query;

        return $query->where('merchant_id', $merchantId);
    }
}
