<?php

namespace App\Exceptions;

class TokenNotFoundException extends BaseException
{
    public function __construct(string $message, int $http_status_code = 400, int $error_code = 1)
    {
        $this->message = $message ?: 'Token not provided';
        $this->http_status_code = $http_status_code;
        $this->error_code = $error_code;
    }
}
