<?php

namespace App\Validators;

use \Prettus\Validator\LaravelValidator;
use \Prettus\Validator\Contracts\ValidatorInterface;

class PaymentValidator extends LaravelValidator 
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'invoice_id' => 'required',
            'item_name'  => 'required',
            'amount'=> 'required',
            'payment_type'=> 'required',
            'customer_name'=> 'required',
            'merchant_id'=> 'required'
        ],
        ValidatorInterface::RULE_UPDATE => [],
    ];
}