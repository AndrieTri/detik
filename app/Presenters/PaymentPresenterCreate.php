<?php

namespace App\Presenters;

use App\Transformers\PaymentTransformerCreate;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class CustomerPresenter.
 *
 * @package namespace App\Presenters;
 */
class PaymentPresenterCreate extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new PaymentTransformerCreate();
    }
}