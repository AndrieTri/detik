<?php

namespace App\Presenters;

use App\Transformers\PaymentTransformerGet;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class CustomerPresenter.
 *
 * @package namespace App\Presenters;
 */
class PaymentPresenterGet extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new PaymentTransformerGet();
    }
}