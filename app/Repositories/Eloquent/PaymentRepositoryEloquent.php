<?php

namespace App\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Contracts\Repositories\PaymentRepository;
use App\Models\Payment;
use App\Validators\PaymentValidator;
use App\Presenters\PaymentPresenter;

/**
 * Class PaymentRepositoryEloquent.
 *
 * @package namespace App\Repositories\Eloquent;
 */
class PaymentRepositoryEloquent extends BaseRepository implements PaymentRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Payment::class;
    }

    private function randomNumber($length) {
        $result = '';

        for($i = 0; $i < $length; $i++) {
            $result .= mt_rand(0, 9);
        }

        return $result;
    }

    public function createCustom(array $request)
    {
        $data = [
            'invoice_id' => $request['invoice_id'],
            'item_name' => $request['item_name'],
            'amount' => $request['amount'],
            'payment_type' => $request['payment_type'],
            'customer_name' => $request['customer_name'],
            'merchant_id' => $request['merchant_id'],
            'status' => "pending",
            'number_va' => $request['payment_type'] == "virtual_account" ? (string) $this->randomNumber(15) : ''
        ];

        $payment = $this->create($data);

        return $payment;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function validator()
    {
        return PaymentValidator::class;
    }
}
