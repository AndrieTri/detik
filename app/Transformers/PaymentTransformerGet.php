<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Payment;
use Carbon\Carbon;

/**
 * Class CustomerInfoTransformer.
 *
 * @package namespace App\Transformers;
 */
class PaymentTransformerGet extends TransformerAbstract
{
    /**
     * Transform the Customer entity.
     *
     * @param \App\Models\Customer $model
     *
     * @return array
     */
    protected $defaultIncludes = [];
    protected $availableIncludes = [];

    public function transform(Payment $model)
    {
        return [
            'references_id'     => (int) $model->references_id,
            'invoice_id'        => (int) $model->invoice_id,
            'status'            => $model->status
        ];
    }
}