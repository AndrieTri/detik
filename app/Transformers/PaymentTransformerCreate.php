<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Payment;
use Carbon\Carbon;

/**
 * Class CustomerInfoTransformer.
 *
 * @package namespace App\Transformers;
 */
class PaymentTransformerCreate extends TransformerAbstract
{
    /**
     * Transform the Customer entity.
     *
     * @param \App\Models\Customer $model
     *
     * @return array
     */
    protected $defaultIncludes = [];
    protected $availableIncludes = [];

    public function transform(Payment $model)
    {
        return [
            'references_id'     => (int) $model->references_id,
            'number_va'         => $model->number_va,
            'status'            => $model->status
        ];
    }
}