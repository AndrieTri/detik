<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class InvoicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('invoices')->truncate();

        $invoices = [
            [
                'invoice_id' => '123',
                'item_name' => 'sepatu',
                'amount' => '1000000',
                'payment_type' => 'virtual_account',
                'customer_name' => 'Andrie',
                'merchant_id' => '1',
                'status' => 'pending',
                'number_va' => '033368842096633',
                'created_date' => Carbon::now()
            ],
            [
                'invoice_id' => '456',
                'item_name' => 'sepatu',
                'amount' => '2000000',
                'payment_type' => 'credit_card',
                'customer_name' => 'Tri',
                'merchant_id' => '2',
                'status' => 'pending',
                'number_va' => '',
                'created_date' => Carbon::now()
            ]
        ];

        foreach($invoices as $invoice){
            DB::table('invoices')->insert($invoice);
        }
    }
}
