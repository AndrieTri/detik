<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class MerchantsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('merchants')->truncate();

        $merchants = [
            [
                'name' => 'Nike',
                'created_date' => Carbon::now()
            ],
            [
                'name' => 'Adidas',
                'created_date' => Carbon::now()
            ],
            [
                'name' => 'New Balance',
                'created_date' => Carbon::now()
            ],
            [
                'name' => 'Diadora',
                'created_date' => Carbon::now()
            ],
            [
                'name' => 'Rebook',
                'created_date' => Carbon::now()
            ]
        ];

        foreach($merchants as $merchant){
            DB::table('merchants')->insert($merchant);
        }
    }
}
