<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableInvoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->bigIncrements('references_id');
            $table->integer('invoice_id');
            $table->string('item_name');
            $table->string('amount');
            $table->enum('payment_type', ['virtual_account','credit_card'])->default('credit_card');
            $table->string('customer_name');
            $table->unsignedInteger('merchant_id');
            $table->enum('status', ['pending','paid','failed'])->default('pending');
            $table->string('number_va');
            $table->datetime('created_date');
            $table->datetime('updated_date')->nullable();
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
